var mymap = L.map('main_map').setView([2.8249300, 41.9831100], 13);
//var mymap = L.map('main_map').setView([3.4699565,-76.4907067], 13);

L.tileLayer('http://{s}title.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors' 
}).addTo(mymap);

L.marker([2.8249300, 41.9831100]).addTo(mymap);
//L.marker([2.8246301, 41.9831101]).addTo(mymap);

/*$.ajax({
    dataType: 'json',
    url: 'api/bicicletas',
    succes: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap);
        });
    }
})*/