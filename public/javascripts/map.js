//var mymap = L.map('main_map').setView([3.4699565,-76.4907067], 13);
var mymap = L.map('main_map').setView([41.98311, 2.82493], 15);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

L.marker([41.98311, 2.82473], { title: 'CC. Chipichape' }).addTo(mymap); 
L.marker([41.98221, 2.8283], { title: 'CC. Unico Outlet' }).addTo(mymap); 
L.marker([41.98431, 2.82593], { title: 'CC. 14 de Calima' }).addTo(mymap); 

$.ajax({
    method: 'POST',
    dataType: 'json',
    url: 'api/auth/authenticate',
    data: { email: 'caov85@gmail.com', password: 'elpotro' },
}).done(function( data ) {
    console.log(data);

    $.ajax({
        dataType: 'json',
        url: 'api/bicicletas',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("x-access-token", data.data.token);
        }
    }).done(function (result) {
        console.log(result);

        result.bicicletas.forEach(bici => {
            L.marker(bici.ubicacion, { title: bici.id }).addTo(mymap);
        });
    });
});