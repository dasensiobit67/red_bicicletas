var usuario=require('../../models/usuario');

exports.usuarios_list=function(req,res){
    usuario.find({},function(err,usuarios){
        res.status(200).json({usuarios:usuarios,});
    });
};

exports.usuarios_create=function(req,res){
    var user = new usuario({nombre: req.body.nombre});
    user.save(function(err){
        res.status(200).json(user);
    });
};

exports.usuario_reservar=function(req,res){
    usuario.findById(req.body.id,function(err,user){
        console.log(user);
        user.reservar(req.body.bici_id,req.body.desde,req.body.hasta,function(err){
            console.log("Reserva!!");
            res.status(200).send();
        });
    });
};