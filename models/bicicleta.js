var Bicicleta=function(id,color,modelo,ubicacion){
    this.id=id;
    this.color=color;
    this.modelo=modelo;
    this.ubicacion=ubicacion;
}

Bicicleta.prototype.toString =function(){
    return 'id: '+this.id + "|color: "+this.color;
}

Bicicleta.allBicis=[];
Bicicleta.add=function(aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById=function(aBiciId){
    var aBiciId=Bicicleta.allBicis.find(x=>x.id == aBiciId);
    if (aBiciId)
        return aBiciId;
    else    
        throw new Error(`No existe la bici con id ${aBiciId}`);
}

Bicicleta.removeById=function(aBiciId){
    console.log("Borrando:"+aBiciId);
    for(var i=0;i<Bicicleta.allBicis.length;i++){
        if(Bicicleta.allBicis[i].id==aBiciId){
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }
    
}

// Modelo bicicletaShema para migrar a MongoDB
var mongoose = require('mongoose');
var Shema = mongoose.Schema;

var bicicletaShema = new Shema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion:{type:[Number], index:{type:'2dsphere',sparse: true}}
});

bicicletaShema.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

bicicletaShema.methods.toString = function(){
    return 'code: '+this.code+' | color: '+this.color;
};

bicicletaShema.statics.allBicis = function(cb){
    return this.find({},cb);
};

bicicletaShema.statics.add = function(aBici,cb){
    this.create(Bici,cb);
};

bicicletaShema.statics.findByCode = function(aCode,cb){
    return this.findONe({code: aCode},cb);
};

bicicletaShema.statics.removeByCode = function(aCode,cb){
    return this.deleteONe({code: aCode},cb);
};
module.exports = mongoose.model('Bicicleta', bicicletaShema);
module.exports = Bicicleta;