var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

describe("Bicicleta API", () =>{
    describe("GET Bicicletas /", () =>{
        it("Status 200",() =>{
            expect(Bicicleta.allBicis.length).toBe(0);
            var a=new Bicicleta(1, "azul", "marina", [2.876543, 41.987654]);
            Bicicleta.add(a);
            request.get('htpps://localhost:3000/bicicletas', function(erro, response, body){
                expect(response.statusCode).toBe(200);
            });

        });
    });

    describe("POST Bicicletas /create", () =>{
        it("Status 200", (done) =>{
            var headers = {'content-type':'aplication/json'};
            var a = {'id': '10', 'color':"azul", 'modelo':"marina", 'lat':'2.876543','lng': '41.987654'};
            request.post({
                headers: headers,
                url: 'htpps://localhost:3000/bicicletas/create', 
                body: a
            }, function(erro, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe('azul');
                done();
            });
        });
    });
});


    
