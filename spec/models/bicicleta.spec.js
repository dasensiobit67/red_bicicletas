const Bicicleta = require('../../models/bicicleta');
var Bicicletas=require('../../models/bicicleta');

beforeEach(() => {Bicicleta.allBicis=[];});

describe("Bicicleta.allBicis", () =>{
    it("comienza vacia",() =>{
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe("Bicicleta.add", () =>{
    it("añadimos una",() =>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var a=new Bicicleta(1, "azul", "marina", [2.876543, 41.987654]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1); 
    });
});

describe("Bicicleta.findById", () =>{
    it("debe devolver bici con id buscado",() =>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var a=new Bicicleta(1, "rojo", "urbana", [2.876543, 41.987654]);
        var b=new Bicicleta(2, "verde", "montaña", [2.876543, 41.987654]);
        Bicicleta.add(a);
        Bicicleta.add(b);
        var targetbici=Bicicleta.findById(2);
        expect(targetbici.id).toBe(2); 
        expect(targetbici.color).toBe(b.color); 
    });
});